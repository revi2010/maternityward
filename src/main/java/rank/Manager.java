package rank;

import static common.SalaryConfig.MANAGMENT_MONTHLY_SALARY;

public class Manager implements Rank {
    @Override
    public double calcSalary(double hours) {
        return MANAGMENT_MONTHLY_SALARY;
    }
}
