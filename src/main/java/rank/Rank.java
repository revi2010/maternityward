package rank;

@FunctionalInterface
public interface Rank {
    double calcSalary(double hours);
}
