package rank;

import static common.SalaryConfig.STANDARD_HOURLY_SALARY;

public class Expert implements Rank {
    @Override
    public double calcSalary(double hours) {
        double hourlySalary = 1.3 * STANDARD_HOURLY_SALARY;
        return hours * hourlySalary;
    }
}
