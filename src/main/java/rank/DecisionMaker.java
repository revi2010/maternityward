package rank;

import static common.SalaryConfig.STANDARD_HOURLY_SALARY;

public class DecisionMaker implements Rank {
    @Override
    public double calcSalary(double hours) {
        int maxHoursCount = 200;
        int minHoursCount = 50;
        double hourlySalary = 1.5 * STANDARD_HOURLY_SALARY;
        return (hours < minHoursCount ? hours : maxHoursCount) * hourlySalary;
    }
}
