package rank;

import static common.SalaryConfig.STANDARD_HOURLY_SALARY;

public class SeniorEmployee implements Rank {
    @Override
    public double calcSalary(double hours) {
        double hourlySalary = 1.05 * STANDARD_HOURLY_SALARY;
        return hours * hourlySalary;
    }
}
