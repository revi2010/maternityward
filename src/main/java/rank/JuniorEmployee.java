package rank;

import static common.SalaryConfig.STANDARD_HOURLY_SALARY;

public class JuniorEmployee implements Rank {
    @Override
    public double calcSalary(double hours) {
        return hours * STANDARD_HOURLY_SALARY;
    }
}
