import common.Employee;
import common.exceptions.InvalidEmployee;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MaternityWard {
    private Map<String, Employee> employees;

    public MaternityWard() {
        this.employees = new HashMap<>();
    }

    public MaternityWard(List<Employee> employees) {
        this.employees = new HashMap<>();
        employees.forEach(employee -> this.employees.put(employee.getId(), employee));
    }

    public MaternityWard(Employee... employees) {
        this.employees = new HashMap<>();
        for (Employee employee: employees) {
            this.employees.put(employee.getId(), employee);
        }
    }

    public Employee getEmployee(String employeeId) {
        if (!this.employees.containsKey(employeeId))
            throw new InvalidEmployee(employeeId, this.getClass());
        return this.employees.get(employeeId);
    }
}
