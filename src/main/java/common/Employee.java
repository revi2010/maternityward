package common;

import position.Position;

public class Employee {
    private String id;
    private String name;
    private Position position;

    public Employee(String id, String name, Position position) {
        this.name = name;
        this.id = id;
        this.position = position;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public Position getPosition() {
        return position;
    }
}
