package common.exceptions;

public class InvalidEmployee extends RuntimeException {
    public InvalidEmployee(String employeeId, Class dataStructure) {
        super(String.format("Employee with id %s does not exist in %s.", employeeId, dataStructure.getName()));
    }
}
