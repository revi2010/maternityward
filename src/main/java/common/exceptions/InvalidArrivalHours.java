package common.exceptions;

public class InvalidArrivalHours extends RuntimeException {
    public InvalidArrivalHours(String message) {
        super(message);
    }
}
