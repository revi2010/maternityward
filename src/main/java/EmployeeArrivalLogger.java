import common.exceptions.InvalidArrivalHours;
import common.exceptions.InvalidEmployee;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class EmployeeArrivalLogger {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private Map<String, Map<LocalDate, Long>> arrivalInfo;

    public EmployeeArrivalLogger() {
        this.arrivalInfo = new HashMap<>();
    }

    public Map<LocalDate, Long> getEmployeeArrivalInfo(String employeeId) {
        if (!arrivalInfo.containsKey(employeeId))
            throw new InvalidEmployee(employeeId, this.getClass());

        return this.arrivalInfo.get(employeeId);
    }

    public Long getEmployeeMonthlyArrivalHours(String employeeId, int month) {
        if (!arrivalInfo.containsKey(employeeId))
            throw new InvalidEmployee(employeeId, this.getClass());

        Map<LocalDate, Long> arrivalInfo = this.arrivalInfo.get(employeeId);
        return arrivalInfo.entrySet().stream()
                .filter(entry -> entry.getKey().getMonthValue() == month)
                .map(Map.Entry::getValue)
                .reduce(0L, Long::sum);
    }

    public void log(String employeeId, LocalDate date, LocalTime arrivalTime, LocalTime departureTime) {
        Long totalWorkingHours = Duration.between(arrivalTime, departureTime).toHours();

        if (arrivalTime.isAfter(departureTime))
            throw new InvalidArrivalHours("Arrival time is after departure time.");

        if (!this.arrivalInfo.containsKey(employeeId) || isNull(this.arrivalInfo.get(employeeId))) {
            HashMap<LocalDate, Long> newLog = new HashMap<>();
            newLog.put(date, totalWorkingHours);
            this.arrivalInfo.put(employeeId, newLog);
        } else {
            this.arrivalInfo.get(employeeId).put(date, totalWorkingHours);
        }
    }

    public void log(String employeeId, String date, String arrivalTime, String departureTime) {
        log(employeeId, LocalDate.parse(date, FORMATTER), LocalTime.parse(arrivalTime), LocalTime.parse(departureTime));
    }
}
