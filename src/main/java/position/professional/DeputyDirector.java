package position.professional;

import rank.DecisionMaker;
import rank.Manager;
import position.Position;

import java.util.List;

public class DeputyDirector extends Position {
    public DeputyDirector() {
        super(List.of(new DecisionMaker(), new Manager()));
    }
}
