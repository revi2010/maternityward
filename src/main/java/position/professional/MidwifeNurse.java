package position.professional;

import rank.SeniorEmployee;

public class MidwifeNurse extends Nurse {
    public MidwifeNurse() {
        super();
        this.ranks.add(new SeniorEmployee());
    }
}
