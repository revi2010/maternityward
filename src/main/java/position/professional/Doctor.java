package position.professional;

import rank.SeniorEmployee;
import position.Position;

public class Doctor extends Position {
    public Doctor() {
        super(new SeniorEmployee());
    }
}
