package position.professional;


import rank.Expert;

public class ExpertDoctor extends SeniorDoctor {
    public ExpertDoctor() {
        super();
        this.ranks.add(new Expert());
    }
}
