package position.professional;

import rank.DecisionMaker;
import rank.Manager;
import position.Position;

import java.util.List;

public class DepartmentDirector extends Position {
    public DepartmentDirector() {
        super(List.of(new Manager(), new DecisionMaker()), 100);
    }
}
