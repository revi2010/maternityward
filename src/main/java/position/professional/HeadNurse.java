package position.professional;

import rank.DecisionMaker;
import rank.SeniorEmployee;

import java.util.List;

public class HeadNurse extends Nurse {
    public HeadNurse() {
        super();
        this.ranks.addAll(List.of(new SeniorEmployee(), new DecisionMaker()));
    }
}
