package position.professional;

import rank.DecisionMaker;

public class SeniorDoctor extends Doctor {
    public SeniorDoctor() {
        super();
        this.ranks.add(new DecisionMaker());
    }
}
