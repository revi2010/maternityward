package position.administrative;

import rank.SeniorEmployee;
import position.Position;

public class CleanersManager extends Position {
    public CleanersManager() {
        super(new SeniorEmployee());
    }
}
