package position.administrative;

import rank.DecisionMaker;
import position.Position;

public class CleanersShiftManager extends Position {
    public CleanersShiftManager() {
        super(new DecisionMaker());
    }
}
