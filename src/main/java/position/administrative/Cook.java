package position.administrative;

import rank.SeniorEmployee;
import position.Position;

public class Cook extends Position {
    public Cook() {
        super(new SeniorEmployee());
    }
}
