package position.administrative;

import rank.DecisionMaker;
import rank.Manager;
import position.Position;

import java.util.List;

public class HeadOfAdministration extends Position {
    public HeadOfAdministration() {
        super(List.of(new Manager(), new DecisionMaker()));
    }
}
