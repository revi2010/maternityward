package position.administrative;

import rank.DecisionMaker;
import rank.Expert;

import java.util.List;

public class Chef extends Cook {
    public Chef() {
        super();
        this.ranks.addAll(List.of(new Expert(), new DecisionMaker()));
    }
}
