package position.administrative;

import rank.DecisionMaker;
import rank.Expert;
import rank.JuniorEmployee;

import java.util.List;

public class HazardousMaterialsCleaner extends Cleaner {
    public HazardousMaterialsCleaner() {
        super();
        this.ranks.addAll(List.of(new JuniorEmployee(), new DecisionMaker(), new Expert()));
        this.riskDegreePercent = 20;
    }
}
