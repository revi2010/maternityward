package position.administrative;

import rank.Expert;

public class AssistantChef extends Cook {
    public AssistantChef() {
        super();
        this.ranks.add(new Expert());
    }
}
