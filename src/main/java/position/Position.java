package position;

import rank.JuniorEmployee;
import rank.Rank;

import java.util.ArrayList;
import java.util.List;

public abstract class Position {
    protected List<Rank> ranks;
    protected double riskDegreePercent = 0;

    public Position() {
        this.ranks = new ArrayList<>(List.of(new JuniorEmployee()));
    }

    public Position(List<Rank> ranks) {
        this.ranks = new ArrayList<>(ranks);
    }

    public Position(Rank rank) {
        this.ranks = new ArrayList<>(List.of(rank));
    }

    public Position(double riskDegreePercent) {
        this.riskDegreePercent = riskDegreePercent;
        this.ranks = new ArrayList<>(List.of(new JuniorEmployee()));
    }

    public Position(List<Rank> ranks, int riskDegreePercent) {
        this.ranks = new ArrayList<>(ranks);
        this.riskDegreePercent = riskDegreePercent;
    }

    public double getRiskDegree() {
        return this.riskDegreePercent;
    }

    /* here the salary calculation logic can be changed */
    public double calcSalary(double hours) {
        double riskBonus = (100 + this.riskDegreePercent) / 100;
        double salary = ranks.stream()
                .mapToDouble(rank -> rank.calcSalary(hours))
                .max()
                .orElse(0);
        return salary * riskBonus;
    }
}
