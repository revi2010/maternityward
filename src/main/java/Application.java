import common.Employee;
import common.exceptions.InvalidArrivalHours;
import common.exceptions.InvalidEmployee;
import position.administrative.AssistantChef;
import position.administrative.HazardousMaterialsCleaner;
import position.administrative.HeadOfAdministration;
import position.professional.BreechBirthInternDoctor;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        MaternityWard maternityWard = new MaternityWard(
                new Employee("1", "revital", new AssistantChef()),
                new Employee("2", "shelly", new HeadOfAdministration()),
                new Employee("3", "galit", new HazardousMaterialsCleaner())
        );

        EmployeeArrivalLogger employeeArrivalLogger = new EmployeeArrivalLogger();
        SalaryCalculator salaryCalculator = new SalaryCalculator(employeeArrivalLogger, maternityWard);

        employeeArrivalLogger.log("1", "09/07/2021", "09:00:00", "11:00:00");
        employeeArrivalLogger.log("2", "09/07/2021", "09:00:00", "15:00:00");
        employeeArrivalLogger.log("3", "09/05/2021", "09:00:00", "11:00:00");

        int choice = 0;
        Scanner sc = new Scanner(System.in);

        while (choice != 3) {
            System.out.println("Choose option: \n" +
                    "1) Add employee arrival hours \n" +
                    "2) Get Employee monthly salary \n" +
                    "3) Exit \n");
            choice = Integer.parseInt(sc.nextLine());

            String id, date, arriveTime, departureTime;

            switch (choice) {
                case 1:
                    System.out.println("Enter employee id: ");
                    id = sc.nextLine();
                    System.out.println("Enter date (dd/MM/yyyy): ");
                    date = sc.nextLine();
                    System.out.println("Enter arrive time (hh:mm:ss): ");
                    arriveTime = sc.nextLine();
                    System.out.println("Enter departure time (hh:mm:ss): ");
                    departureTime = sc.nextLine();
                    try {
                        employeeArrivalLogger.log(id, date, arriveTime, departureTime);
                    } catch (InvalidArrivalHours ex) {
                        System.out.println(ex.getMessage());
                    };
                    break;

                case 2:
                    System.out.println("Enter employee id: ");
                    id = sc.nextLine();
                    try {
                        System.out.println(salaryCalculator.getMonthlySalaryOf(id));
                    } catch (InvalidEmployee ex) {
                        System.out.println(ex.getMessage());
                    };
                    break;
            }
        }
    }
}
