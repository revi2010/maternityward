import common.Employee;

import java.time.LocalDate;
import java.util.HashMap;

public class SalaryCalculator {
    private EmployeeArrivalLogger employeeArrivalLogger;
    private MaternityWard maternityWard;

    public SalaryCalculator(EmployeeArrivalLogger employeeArrivalLogger, MaternityWard maternityWard) {
        this.employeeArrivalLogger = employeeArrivalLogger;
        this.maternityWard = maternityWard;
    }

    public double getMonthlySalaryOf(String employeeId) {
        int month = LocalDate.now().getMonthValue();
        Employee employee = this.maternityWard.getEmployee(employeeId);
        Long hours = this.employeeArrivalLogger.getEmployeeMonthlyArrivalHours(employeeId, month);
        return employee.getPosition().calcSalary(hours);
    }
}
